<?php
get_header(); 
$detect = new WP_Rocket_Mobile_Detect; ?>

<main id="primary" class="site-main category-container">
  <!-- Desktop -->
  <div class="hidden md:block">
    <div class="wrap mt-5"> <?php
      if (!$detect->isMobile()) {
        np_template_desktop('category');
      } ?>
    </div>
  </div>

  <!-- Mobile -->
  <div class="block md:hidden"> <?php
    if ($detect->isMobile()) {
      np_template_mobile('category');
    } ?>
  </div>
</main><!-- #main -->

<?php
get_footer();
