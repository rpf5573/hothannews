<footer class="site-footer p-6 md:p-14">
  <div class="wrap">
    <div class="site-info flex justify-between items-center">
      <div class="flex flex-col gap-y-4 items-center md:flex-row md:gap-y-0 md:justify-start">
        <img src="<?php echo np_get_site_footer_logo_url(); ?>" alt="hothannews-footer-logo" class="w-[80px] mr-5">
        <div class="flex flex-col justify-center text-gray-800">
          <?php echo np_get_site_footer_info(); ?>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>

<script>
  setTimeout(() => {
    feather.replace();
  }, 1000);
</script>
<?php wp_footer(); ?>
</body>

</html>
