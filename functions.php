<?php

require_once get_template_directory() . '/inc/class-ajax.php';
new NP_Ajax();

require_once get_template_directory() . '/inc/class-admin-traffic.php';
new NP_Admin_Traffic();

$mobile_detect_path = ABSPATH . 'wp-content/plugins/wp-rocket/inc/classes/dependencies/mobiledetect/mobiledetectlib/Mobile_Detect.php';
if ( file_exists( $mobile_detect_path ) ) {
  // Ooh la la! File exists, let's include it
  require_once( $mobile_detect_path );
} else {
  // Uh-oh, file doesn't exist, better send a message
  function my_admin_notice__error() {
      $class = 'notice notice-error';
      $message = __( 'Mobile_Detect.php file not found. Please check the path or if the file exists.', 'my_text_domain' );

      printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
  }
  add_action( 'admin_notices', 'my_admin_notice__error' );
}


function np_setup() {
  add_theme_support('automatic-feed-links');
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');
  add_theme_support(
    'html5',
    array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
      'style',
      'script',
    )
  );

  // Add support for Block Styles.
  add_theme_support( 'wp-block-styles' );

  // Add support for full and wide align images.
  add_theme_support( 'align-wide' );

  // Add support for editor styles.
  add_theme_support( 'editor-styles' );

  add_theme_support('menus');
  register_nav_menus(
    array(
      'primary-menu' => 'Main Menu',
    )
  );
  register_nav_menus(
    array(
      'mobile-menu' => 'Mobile Menu',
    )
  );

  add_image_size( 'np-size-400x300', 400, 300, true );
  add_image_size( 'np-size-200x150', 200, 150, true );
  add_image_size( 'np-size-300x200', 300, 200, true );
  add_image_size( 'np-size-300x300', 300, 300, true );
  add_image_size( 'np-size-1600x400', 1600, 400, true );
  add_image_size( 'np-size-250x150', 250, 150, true );
  add_image_size( 'np-size-150x150', 150, 150, true );
}
add_action('after_setup_theme', 'np_setup');

function np_scripts() {
  $random = rand(1, 5000);
  // 개발용 테일윈드
  // wp_enqueue_script( 'tailwind', 'https://cdn.tailwindcss.com' );
  // wp_enqueue_style('tachyons', 'https://unpkg.com/tachyons@4.12.0/css/tachyons.min.css', array(), $random);

	wp_enqueue_style( 'hothannews-tailwind', np_asset( 'build/tailwind/tailwind.css' ), array(), '1.0.0' );
  wp_enqueue_style( 'hothannews-main', np_asset( 'build/scss/style.css' ), array(), '1.0.0' );
  wp_enqueue_style( 'hothannews-style', get_stylesheet_uri(), array(), '1.0.0' );
  
  wp_enqueue_script( 'swiper', np_asset( 'assets/js/vendor/swiper.js' ), array(), '9.4', true );
  wp_enqueue_script( 'mmenu', np_asset( 'assets/js/vendor/mmenu-light.js' ), array(), '1.0.0', true );
  wp_enqueue_script( 'micromodal', np_asset( 'assets/js/vendor/micromodal.js' ), array(), '1.0.0', true );

  wp_enqueue_script( 'hothannews-app', np_asset( 'build/js/app-min.js' ), array('jquery', 'swiper', 'mmenu', 'micromodal'), '1.0.0', true );

  wp_localize_script( 'hothannews-app', 'np_ajax_object', array( 
    'ajaxurl' => admin_url( 'admin-ajax.php' )
  ));
}
add_action('wp_enqueue_scripts', 'np_scripts');

add_filter( 'body_class', function($classes) {
  $user_id = get_current_user_id();
  if (!$user_id) return $classes;

  $should_show = np_get_section_label_onoff();
  if (!$should_show) {
    return $classes;
  }

  $role_list = [
    'administrator',
    'chief_executive_officer',
    'editor_in_chief',
    'deputy_editor'
  ];

  $user = get_userdata($user_id);
  $user_roles = $user->roles;
  foreach($role_list as $role) {
    if (in_array($role, $user_roles)) {
      $classes[] = 'show-section';
      return $classes;
    }
  }
  
  $classes[] = 'show-section';
  return $classes;
});

add_filter( 'user_has_cap', function($allcaps, $cap, $args) {
  if(is_admin() && isset($args[0]) && ($args[0] == 'edit_post' || $args[0] == 'delete_post')) {
    $current_user = wp_get_current_user();
    if(in_array('section_chief', $current_user->roles)) {
      $post = get_post($args[2]);
      if (!$post) return $allcaps;

      // 자신이 쓴 글이면 수정할 수 있다
      $user_id = $current_user->ID;
      if ($post->post_author == $user_id) {
        return $allcaps;
      }

      // 자신에게 속한 팀원이 쓴 글이면 수정할 수 있다
      $team_member_list = rwmb_get_value( 'team_member_list', [ 'object_type' => 'user' ], $user_id );
      if(!in_array($post->post_author, $team_member_list)) {
        $allcaps[$cap[0]] = false;
        return $allcaps;
      }
    }
  }
  return $allcaps;
}, 9999, 3);

function np_asset( $path ) {
	if ( wp_get_environment_type() === 'production' ) {
		return get_stylesheet_directory_uri() . '/' . $path;
	}

	return add_query_arg( 'time', time(),  get_stylesheet_directory_uri() . '/' . $path );
}

function np_template_common($slug, $args = array()) {
  get_template_part( "template-parts/common/" . $slug, '', $args );
}

function np_template_desktop($slug, $args = array()) {
  get_template_part( "template-parts/desktop/" . 'desktop-' . $slug, '', $args );
}

function np_template_mobile($slug, $args = array()) {
  get_template_part( "template-parts/mobile/" . 'mobile-' . $slug, '', $args );
}

function np_get_latest_posts($posts_per_page = 10, $page = 1, $category = null, $author = null) {
  $args = array(
    'post_type'      => 'post',
    'post_status'    => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged'          => $page,
    'category_name'  => $category,
    'author'         => $author,
    'orderby'        => 'date',
    'order'          => 'DESC',
  );

  // It's time to tidy up! If no values were passed in, we'll take them out. 
  // Keeping your code clean is like keeping your room clean - makes it easier to find stuff!

  foreach($args as $key => $value){
    if(!$value) unset($args[$key]);
  }

  $query = new WP_Query($args);

  $posts = array();

  if ($query->have_posts()) {
    while($query->have_posts()){
      $query->the_post();

      $posts[] = array(
        'title' => get_the_title(),
        'content' => get_the_content(),
        'author' => get_the_author(),
        'date' => get_the_date(),
        'thumbnail_image_id' => get_post_thumbnail_id(),
        'post_link' => get_permalink()
      );
    }

    wp_reset_postdata();
  }

  return $posts;
}

function np_get_popular_posts($count = 10, $hours = 24, $category_id_list = [], $author_id_list = []) {
  global $wpdb ;
  if (!defined('APVC_DATA_TABLE')) {
    return array();
  }

  // Calculating the date $days ago from today
  $startDate = date( 'Y-m-d H:i:s',strtotime( date( 'Y-m-d H:i:s' ) . " -{$hours} hour" ) );
  $endDate = date('Y-m-d H:i:s');

  // Preparing SQL query
  $sql = "
      SELECT ap.article_id, COUNT(*) as visit_count
      FROM " . APVC_DATA_TABLE . " as ap
      INNER JOIN {$wpdb->posts} as p ON ap.article_id = p.ID
      LEFT JOIN {$wpdb->term_relationships} as tr ON p.ID = tr.object_id
      WHERE (ap.date BETWEEN %s AND %s) AND (p.post_status = 'publish') AND (p.post_type = 'post') ";

  // Adding category condition
  if (!empty($category_id_list)) {
    $sql .= "AND tr.term_taxonomy_id IN (" . implode(',', $category_id_list) . ") ";
  }

  // Adding author condition
  if (!empty($author_id_list)) {
    $sql .= "AND p.post_author IN (" . implode(',', $author_id_list) . ") ";
  }

  $sql .= "GROUP BY ap.article_id
           ORDER BY visit_count DESC
           LIMIT %d";

  // Creating an array of parameters for prepare()
  $params = [$startDate, $endDate];
  if(isset($category)) $params[] = $category;
  if(isset($author_id)) $params[] = $author_id;
  $params[] = $count;

  $prepared_sql = $wpdb->prepare($sql, $params);

  // Preparing, executing the SQL query, and getting results
  $results = $wpdb->get_results($prepared_sql);

  // Extracting article IDs
  $article_ids = array_map(function($item){
      return $item->article_id;
  }, $results);

  // Extracting visit counts
  $visit_counts = array_map(function($item){
    return $item->visit_count;
  }, $results);

  $args = array(
    'post_type'      => 'post',
    'post_status'    => 'publish',
    'posts_per_page' => $count,
    'post__in'       => $article_ids,
  );

  $query = new WP_Query($args);

  $posts = array();
  if ($query->have_posts()) {
    $i = 0;
    while($query->have_posts()){
      $query->the_post();
      $posts[] = array(
        'title' => get_the_title(),
        'content' => get_the_content(),
        'author' => get_the_author(),
        'date' => get_the_date(),
        'thumbnail_image_id' => get_post_thumbnail_id(),
        'post_link' => get_permalink(),
        'visit_count' => $visit_counts[$i],
      );
      $i += 1;
    }

    wp_reset_postdata();
  }

  return $posts;
}

function np_get_image_url($image_id, $size = 'full') {
  if (!$image_id) {
    return 'https://placehold.co/300x300';
  }
  return wp_get_attachment_image_src($image_id, $size)[0];
}

function np_get_image_alt_text($image_id) {
  if (!$image_id) {
    return 'no-image-alt';
  }
  return get_post_meta($image_id, '_wp_attachment_image_alt', true) ?: 'no-image-alt';
}

function np_get_category_archive_url_by_slug($category_slug) {
  // Fetch the category by the slug.
  $category = get_category_by_slug($category_slug);

  // Check if the category exists.
  if ($category) {
      // Get the URL of the category archive.
      $category_link = get_category_link($category->term_id);

      return $category_link;
  } else {
      return '/';
  }
}

function np_category_breadcrumbs($post_id) {
  // Start output buffering.
  ob_start();

  // Get the current category ID.
  $categories = wp_get_post_categories($post_id); 
  if (count($categories) === 0) return ''; // 카테고리가 없는 경우를 대비한다

  $category = get_category($categories[0]); 
  $link = get_category_link($category->term_id); ?>

  <ul class="flex gap-x-2 text-sm">
    <li>
      <a href="<?php echo get_home_url(); ?>">
        <i class="icon-home"></i> 홈 >
      </a>
    </li>
    <li>
      <a href="<?php echo $link; ?>">
        <i class="icon-home"></i> <?php echo $category->name; ?>
      </a>
    </li>
  </ul> <?php

  return ob_get_clean();
}

function np_category_pagination() {
  global $wp_query;

  $big = 999999999;

  echo paginate_links( array(
      'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format'  => '?paged=%#%',
      'current' => max( 1, get_query_var('paged') ),
      'total'   => $wp_query->max_num_pages,
      'prev_text'    => '<',
	    'next_text'    => '>',
	    'type'         => 'plain',
  ) );
}

function np_get_comment_meta($comment_id) {
  $comment = get_comment($comment_id);
  if (is_null($comment) || $comment->comment_approved != '1') {
    return new WP_Error('invalid_comment', '검토중인 댓글입니다');
  }

  $content = $comment->comment_content;
  $username = rwmb_get_value('comment_username', ['object_type' => 'comment'], $comment_id);
  $password = rwmb_get_value('comment_password', ['object_type' => 'comment'], $comment_id);
  $ip = rwmb_get_value('comment_ip', ['object_type' => 'comment'], $comment_id);
  $like_count = rwmb_get_value('comment_like', ['object_type' => 'comment'], $comment_id);
  $dislike_count = rwmb_get_value('comment_dislike', ['object_type' => 'comment'], $comment_id);

  return [
    'comment_id' => $comment_id,
    'content' => $content,
    'username' => $username,
    'password' => $password,
    'ip' => $ip ? $ip : '0.0.0.0',
    'like' => $like_count ? $like_count : 0,
    'dislike' => $dislike_count ? $dislike_count : 0,
  ];
}

function np_get_comment_id_list($post_id) {
  global $wpdb;

  $query = "
    SELECT comment_ID
    FROM $wpdb->comments
    WHERE comment_post_ID = %d
    AND comment_approved = '1'
    AND comment_parent = 0
    ORDER BY comment_date DESC
  ";

  $comment_ids = $wpdb->get_col( $wpdb->prepare( $query, $post_id ) );
  return $comment_ids;
}

function sort_users_by_role($a, $b) {
  $role_order = [
    'administrator',
    'chief_executive_officer',
    'editor_in_chief',
    'deputy_editor',
    'section_chief',
    'staff_reporter',
  ];

  $a_role = $a->roles[0];
  $b_role = $b->roles[0];

  $a_index = array_search($a_role, $role_order);
  $b_index = array_search($b_role, $role_order);

  if ($a_index === false) $a_index = count($role_order);
  if ($b_index === false) $b_index = count($role_order);

  return $a_index - $b_index;
}

function np_get_site_logo_url() {
  $image = rwmb_meta( 'site_logo', [ 'object_type' => 'setting', 'size' => 'full' ], 'newspaper_customizer_setting' );
  return $image['url'];
}

function np_get_site_footer_logo_url() {
  $image = rwmb_meta( 'site_footer_logo', [ 'object_type' => 'setting', 'size' => 'thumbnail' ], 'newspaper_customizer_setting' );
  return $image['url'];
}

function np_get_copyright_message() {
  $value = rwmb_meta( 'single_copyright_message', [ 'object_type' => 'setting' ], 'newspaper_customizer_setting' );
  return $value;
}

function np_get_site_footer_info() {
  $value = rwmb_meta( 'footer_site_info', [ 'object_type' => 'setting' ], 'newspaper_customizer_setting' );
  return $value;
}

function np_get_ad_1() {
  $value = rwmb_meta( 'ad_1', [ 'object_type' => 'setting' ], 'newspaper_customizer_setting' );
  return $value;
}

function np_get_ad_2() {
  $value = rwmb_meta( 'ad_2', [ 'object_type' => 'setting' ], 'newspaper_customizer_setting' );
  return $value;
}

function np_get_section_label_onoff() {
  $value = rwmb_meta( 'section_label_on_off', [ 'object_type' => 'setting' ], 'newspaper_customizer_setting' );
  return !!$value;
}

function np_get_traffic_open_to_all() {
  $value = rwmb_meta( 'traffic_open_to_all', [ 'object_type' => 'setting' ], 'newspaper_customizer_setting' );
  return !!$value;
}

function np_get_team_member_list($user_id) {
  $team_member_id_list = rwmb_get_value( 'team_member_list', [ 'object_type' => 'user' ], $user_id ) ?: [];

  return array_map(function($id) { return intval($id); }, $team_member_id_list);
}

function np_is_section_chief($user_id) {
  $user = get_user_by( 'ID', $user_id );
  if (in_array('section_chief', $user->roles )) {
    return true;
  }
  return false;
}

function np_get_post_reaction_count($post_id, $reaction) {
  $field_id = 'post_reaction_' . $reaction;
  $value = rwmb_get_value( 'post_reaction_' . $reaction, '', $post_id ) ?: 0;
  return $value;
}
