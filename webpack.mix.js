let mix = require('laravel-mix');
let path = require('path');

mix.setResourceRoot('../');
mix.setPublicPath(path.resolve('./'));

mix.sass('assets/scss/style.scss', 'build/scss');
mix.postCss("assets/css/tailwind.css", "build/tailwind");

if (mix.inProduction()) {
    mix.version();
} else {
    Mix.manifest.refresh = _ => void 0
}
