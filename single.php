<?php
get_header();
$detect = new WP_Rocket_Mobile_Detect; ?>

<main id="primary" class="site-main">
  <!-- Desktop -->
  <div class="hidden md:block"> <?php
    if (!$detect->isMobile()) {
      np_template_desktop('single');
    } ?>
  </div>

  <!-- Mobile -->
  <div class="block md:hidden"> <?php
    if ($detect->isMobile()) {
      np_template_mobile('single');
    } ?>
  </div>
</main><!-- #main -->

<?php
  np_template_common('remove-comment-modal');
  np_template_common('remove-reply-modal');
?>

<?php
get_footer();
