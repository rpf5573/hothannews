<?php
get_header();
$detect = new WP_Rocket_Mobile_Detect;
?>

<main id="primary" class="site-main">
  <!-- Desktop -->
  <div class="hidden md:block"> <?php
    if (!$detect->isMobile()) {
      np_template_desktop('front-page');
    } ?>
  </div>

  <!-- Mobile -->
  <div class="block md:hidden"> <?php
    if ($detect->isMobile()) {
      np_template_mobile('front-page');
    } ?>
  </div>
</main><!-- #main -->

<?php
get_footer();
