// 모바일에서만 작동한다
if (window.innerWidth < 768) {
  document.addEventListener("DOMContentLoaded", () => {
    (($) => {
      new Swiper(".mobile-front-section-1 .swiper", {
        // Optional parameters
        loop: true,
        autoplay: {
          delay: 5000,
          speed: 1000,
        },
        spaceBetween: 20,

        // If we need pagination
        pagination: {
          el: document.querySelector(".mobile-front-section-1 .swiper-pagination"),
          clickable: true,
        },
      });
    })(jQuery);

    (($) => {
      new Swiper(".mobile-front-section-2 .swiper", {
        // Optional parameters
        loop: true,
        autoplay: {
          delay: 5000,
          speed: 1000,
        },
        spaceBetween: 20,

        // If we need pagination
        pagination: {
          el: document.querySelector(".mobile-front-section-2 .swiper-pagination"),
          clickable: true,
        },
      });
    })(jQuery);

    (($) => {
      new Swiper('.mobile-front-section-3 .swiper', {
        loop: true,
        autoplay: {
          delay: 5000,
          speed: 1000,
        },
        spaceBetween: 20,

        pagination: {
          el: document.querySelector('.mobile-front-section-3 .swiper-pagination'),
          clickable: true,
        },
      });
    })(jQuery);

    (($) => {
      new Swiper('.mobile-front-section-4 .swiper', {
        loop: true,
        autoplay: {
          delay: 5000,
          speed: 1000,
        },
        spaceBetween: 20,

        pagination: {
          el: document.querySelector('.mobile-front-section-4 .swiper-pagination'),
          clickable: true,
        },
      });
    })(jQuery);
  });
}
