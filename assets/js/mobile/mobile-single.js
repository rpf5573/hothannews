// 모바일에서만 작동한다
if (window.innerWidth < 768) {
  (($) => {
    new Swiper(".mobile-single-section-4 .swiper", {
      // Optional parameters
      loop: true,
      autoplay: {
        delay: 5000,
        speed: 1000,
      },
      spaceBetween: 20,

      // If we need pagination
      pagination: {
        el: document.querySelector(".mobile-single-section-4 .swiper-pagination"),
        clickable: true,
      },
    });

    new Swiper(".mobile-single-section-5 .swiper", {
      // Optional parameters
      loop: true,
      autoplay: {
        delay: 5000,
        speed: 1000,
      },
      spaceBetween: 20,

      // If we need pagination
      pagination: {
        el: document.querySelector(".mobile-single-section-5 .swiper-pagination"),
        clickable: true,
      },
    });

    new Swiper(".mobile-single-section-7 .swiper", {
      // Optional parameters
      loop: true,
      autoplay: {
        delay: 5000,
        speed: 1000,
      },
      spaceBetween: 20,

      // If we need pagination
      pagination: {
        el: document.querySelector(".mobile-single-section-7 .swiper-pagination"),
        clickable: true,
      },
    });
  })(jQuery);
}