if (window.innerWidth < 768) {
  // 더보기 ajax loading
  (($) => {
    $('.mobile-category-section-1 .load-more').click(function() {
      const btn = $(this);
      const nextPage = Number(btn.data('current-page')) + 1;
      const category_id = btn.data('category-id');
      btn.addClass('loading');

      $.ajax({
          url : np_ajax_object.ajaxurl,
          data : {
            'action': 'mobile_catgory_list_load_more',
            'page' : nextPage,
            'category_id' : category_id
          },
          type : 'POST',
          success : function( response ){
            const { posts, more } = response.data;
            btn.data('current-page', nextPage);
            $('.mobile-category-article-list').append(posts);

            if (!more) {
              btn.remove();
              $('.mobile-category-section-1 .no-more-posts').removeClass('hidden');
            }
            btn.removeClass('loading');
          },
          error: function( error ) {
            alert("데이터를 불러오는데 실패했습니다");
            // 새로고침
            location.reload();
          }
      });
    });
  })(jQuery);

  // 카테고리 페이지 최신 뉴스
  (($) => {
    new Swiper('.mobile-category-section-2 .swiper', {
      loop: true,
      autoplay: {
        delay: 5000,
        speed: 1000,
      },
      spaceBetween: 20,

      pagination: {
        el: document.querySelector('.mobile-category-section-2 .swiper-pagination'),
        clickable: true,
      },
    });
  })(jQuery);

  // 카테고리 24시간 인기 뉴스
  (($) => {
    new Swiper('.mobile-category-section-3 .swiper', {
      loop: true,
      autoplay: {
        delay: 5000,
        speed: 1000,
      },
      spaceBetween: 20,

      pagination: {
        el: document.querySelector('.mobile-category-section-3 .swiper-pagination'),
        clickable: true,
      },
    });
  })(jQuery);
}
