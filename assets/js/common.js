// mmenu
(($) => {
  document.addEventListener("DOMContentLoaded", () => {
    const $menu = document.querySelector(".offset-menu");
    const $trigger = document.querySelector("#offset-menu-trigger");
    if ($menu === null || $trigger === null) return;

    const menu = new MmenuLight($menu);
    const navigator = menu.navigation();
    const drawer = menu.offcanvas();

    $trigger.addEventListener("click", (evnt) => {
      evnt.preventDefault();
      drawer.open();
    });
  });
})(jQuery);

// 댓글
document.addEventListener("DOMContentLoaded", () => {
  // 상세페이지 댓글
  (($) => {
    const $form = $('.single-post .comment-form');

    // counter
    const maxLength = 400;
    const $textarea = $('.single-post .comment-form textarea');
    const $currentLetterCounter = $($form.find('.letter-counter .current-count'));

    $textarea.on('input', (e) => {
      const $self = $(e.currentTarget);
      const currentLength = $self.val().length;
      if (currentLength > maxLength) {
        // Trim the field current field value to the max length
        $textarea.val($self.val().substring(0, maxLength));
      }
      $currentLetterCounter.html(`${$self.val().length}`);
    });

    // submit
    $form.on('submit', (e) => {
      e.preventDefault();
      const $self = $(e.currentTarget);

      const $submitButton = $($self.find('button[type="submit"]'));
      $submitButton.prop('disabled', true);

      const username = $self.find('input[name="username"]').val();
      const password = $self.find('input[name="password"]').val();
      const content = $self.find('textarea[name="content"]').val();
      const post_id = $self.find('input[name="post_id"]').val();
      const nonce = $self.find('input[name="comment_nonce"]').val();

      const data = {
        'action': 'custom_comment',
        'username': username,
        'password': password,
        'content': content,
        'post_id': post_id,
        'comment_nonce': nonce,
      };

      $.post(np_ajax_object.ajaxurl, data, function(response) {
        alert(response.data);
        location.reload();
      });
    });

    $('.show-reply-form-button').on('click', (e) => {
      const $self = $(e.currentTarget);
      const $replyForm = $($self.closest('.comment-list-item').find('.reply-list'));
      $replyForm.toggleClass('hidden');

      if (!$replyForm.hasClass('hidden')) {
        $('html, body').animate({
          scrollTop: $replyForm.offset().top - 100
        }, 600);
      }
    });
  })(jQuery);

  // 상세페이지 대댓글
  (($) => {
    const $form = $('.single-post .reply-form');

    // counter
    const maxLength = 400;
    const $textarea = $($form.find('textarea'));
    
    $textarea.on('input', (e) => {
      const $self = $(e.currentTarget);
      const $letterCounter = $($self.closest('.reply-form').find('.letter-counter .current-count'));
      const currentLength = $self.val().length;
      if (currentLength > maxLength) {
        $self.val($self.val().substring(0, maxLength));
      }
      $letterCounter.html(`${$self.val().length}`);
    });

    // submit
    $form.on('submit', (e) => {
      e.preventDefault();

      const $self = $(e.currentTarget);
      const $submitButton = $($self.find('button[type="submit"]'));
      $submitButton.prop('disabled', true);

      const username = $self.find('input[name="username"]').val();
      const password = $self.find('input[name="password"]').val();
      const content = $self.find('textarea[name="content"]').val();
      const post_id = $self.find('input[name="post_id"]').val();
      const parent_comment_id = $self.find('input[name="parent_comment_id"]').val();
      const nonce = $self.find('input[name="reply_nonce"]').val();

      const data = {
        'action': 'custom_reply',
        'username': username,
        'password': password,
        'content': content,
        'post_id': post_id,
        'parent_comment_id': parent_comment_id,
        'reply_nonce': nonce,
      };

      $.post(np_ajax_object.ajaxurl, data, function(response) {
        alert(response.data);
        location.reload();
      });
    });
  })(jQuery);

  // comment 삭제
  (($) => {
    const $removeCommentButton = $('.remove-comment-button');
    if ($removeCommentButton.length === 0) return;

    let commentId = null;

    $removeCommentButton.on('click', function (e) {
      const $self = $(e.currentTarget);
      commentId = Number($self.data('comment-id'));
      if (!commentId) return;

      MicroModal.show('remove-comment-modal', {
        onShow: (modal) => {
          $('#remove-comment-modal input[name="password"]').val('');
        },
        onClose: (modal) => {
          commentId = null;
        }
      });
    });

    $('#remove-comment-modal button.modal__btn-primary').on('click', function (e) {
      if (commentId === null) return;

      const $self = $(e.currentTarget);
      $self.prop('disabled', true);

      const password = $('#remove-comment-modal input[name="password"]').val();
      if (!password) {
        alert("비밀번호를 입력해 주세요");
        $self.prop('disabled', false);
        return;
      }

      $.post(np_ajax_object.ajaxurl, {
        action: 'remove_comment',
        comment_id: commentId,
        password,
      }).done((res) => {
        $self.prop('disabled', false);

        if (!res.success) {
          alert(res.data);
          return;
        }
        MicroModal.close('remove-comment-modal');
        alert(res.data);
        location.reload();
      });
    });

  })(jQuery);

  // reply 삭제
  (($) => {
    const $removeReplyButton = $('.remove-reply-button');
    if ($removeReplyButton.length === 0) return;

    let replyId = null;

    $removeReplyButton.on('click', function (e) {
      const $self = $(e.currentTarget);
      replyId = Number($self.data('reply-id'));
      if (!replyId) return;

      MicroModal.show('remove-reply-modal', {
        onShow: (modal) => {
          $('#remove-reply-modal input[name="password"]').val('');
        },
        onClose: (modal) => {
          replyId = null;
        }
      });
    });

    $('#remove-reply-modal button.modal__btn-primary').on('click', function (e) {
      if (replyId === null) return;

      const $self = $(e.currentTarget);
      $self.prop('disabled', true);

      const password = $('#remove-reply-modal input[name="password"]').val();
      if (!password) {
        alert("비밀번호를 입력해 주세요");
        $self.prop('disabled', false);
        return;
      }

      $.post(np_ajax_object.ajaxurl, {
        action: 'remove_reply',
        reply_id: replyId,
        password,
      }).done((res) => {
        $self.prop('disabled', false);

        if (!res.success) {
          alert(res.data);
          return;
        }
        MicroModal.close('remove-reply-modal');
        alert(res.data);
        location.reload();
      });
    });
  })(jQuery);
});

// 감정표현
document.addEventListener('DOMContentLoaded', () => {
  (($) => {
    const $emojiBtns = $('button.emoji-btns');
    if ($emojiBtns.length === 0) return;

    $emojiBtns.on('click', (e) => {
      const $self = $(e.currentTarget);
      const $reactionCount = $self.find('.reaction-count');
      const $indicator = $self.find('.reaction-loading-indicator');

      const reaction = $self.data('reaction');
      if (!reaction) return;

      const post_id = $self.data('post_id');
      if (!post_id) return;

      const nonce = $('input#post_reaction_nonce').val();
      if (!nonce) return;

      // localStorage에 reaction과 post_id를 조합해서 저장한다
      const key = `newspaper_${reaction}_${post_id}`;
      const value = localStorage.getItem(key);
      const undo = value ? true : false;

      $self.prop('disabled', true);
      $reactionCount.toggleClass('hidden');
      $indicator.toggleClass('hidden');

      $.post(np_ajax_object.ajaxurl, {
        action: 'update_post_reaction',
        reaction,
        post_id,
        nonce,
        undo,
      }).done((res) => {
        $self.prop('disabled', false);

        $reactionCount.toggleClass('hidden');
        $indicator.toggleClass('hidden');

        if (!res.success) {
          alert(res.data);
          return;
        }

        const count = parseInt($reactionCount.text(), 10);
        if (undo) {
          $reactionCount.text(count - 1);
          localStorage.removeItem(key);
        } else {
          $reactionCount.text(count + 1);
          localStorage.setItem(key, true);
        }
      });
    });
  })(jQuery);
});


console.log('common is loaded');