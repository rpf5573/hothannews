if (window.innerWidth >= 768) {
  document.addEventListener('DOMContentLoaded', () => {
    // 상세페이지 많이본 뉴스
    (($) => {
      new Swiper('.desktop-single-section-4 .swiper', {
        loop: false,
        spaceBetween: 20,
        pagination: {
          el: document.querySelector('.desktop-single-section-4 .swiper-pagination'),
          clickable: true,
        },
      });
    })(jQuery);

    // 실시간 인기 뉴스
    (($) => {
      new Swiper('.desktop-single-section-7 .swiper', {
        loop: true,
        spaceBetween: 20,
        pagination: {
          el: document.querySelector('.desktop-single-section-7 .swiper-pagination'),
          clickable: true,
        },
      });
    })(jQuery);

    (($) => {
      new Swiper('.desktop-single-section-8 .swiper', {
        loop: true,
        autoplay: {
          delay: 5000,
          speed: 1000,
        },
        spaceBetween: 20,

        pagination: {
          el: document.querySelector('.desktop-single-section-8 .swiper-pagination'),
          clickable: true,
        },
      });
    })(jQuery);
  });
}
