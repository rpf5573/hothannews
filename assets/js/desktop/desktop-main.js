if (window.innerWidth >= 768) {
  // menu
  (($) => {
    document.addEventListener("DOMContentLoaded", function () {
      (($) => {
        // Navigation drop down effect
        $("#main-nav ul.sub-menu").css({
          display: "none",
        });

        $("#main-nav li").hover(
          function () {
            $(this)
              .find("ul:first")
              .css({
                visibility: "visible",
                display: "none",
              })
              .fadeIn(300);
          },
          function () {
            $(this).find("ul:first").css({
              visibility: "visible",
              display: "none",
            });
          }
        );

        // search form
        $(".search-trigger").on("click", function (e) {
          e.preventDefault();
          $(this).parent().find(".search-form").toggleClass("search-active");
          $(this).toggleClass("search-btn-active");
          $(this).parent().find(".search-form input.search-field").focus();
        });
      })(jQuery);
    });
  })(jQuery);
}
