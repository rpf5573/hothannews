if (window.innerWidth >= 768) {
  // desktop-front-section-2
  (($) => {
    // 실시간 인기 뉴스
    new Swiper('.desktop-front-section-2 .swiper', {
      loop: false,
      spaceBetween: 20,
      pagination: {
        el: document.querySelector('.desktop-front-section-2 .swiper-pagination'),
        clickable: true,
      },
    });
  })(jQuery);

  // desktop-front-section-3
  (($) => {
    new Swiper('.desktop-front-section-3 .swiper', {
      loop: false,
      spaceBetween: 20,
      pagination: {
        el: document.querySelector('.desktop-front-section-3 .swiper-pagination'),
        clickable: true,
      },
    });
  })(jQuery);

  // 더보기 ajax loading
  (($) => {
    $('.desktop-front-section-4 .load-more').click(function() {
      const btn = $(this);
      const nextPage = Number(btn.data('current-page')) + 1;

      btn.addClass('loading');

      $.ajax({
          url : np_ajax_object.ajaxurl,
          data : {
            'action': 'desktop_section_4_load_more',
            'page' : nextPage,
          },
          type : 'POST',
          success : function( response ){
            const { posts, more } = response.data;
            btn.data('current-page', nextPage);
            $('.desktop-front-section-4 > ul').append(posts);

            if (!more) {
              btn.remove();
              $('.desktop-front-section-4 .no-more-posts').removeClass('hidden');
            }

            btn.removeClass('loading');
          },
          error: function( error ) {
            alert("데이터를 불러오는데 실패했습니다");
            // 새로고침
            location.reload();
          }
      });
    });
  })(jQuery);
}


console.log('desktop-front is loaded');