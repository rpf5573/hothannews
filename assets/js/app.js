import './common.js';

import './desktop/desktop-main.js';
import './desktop/desktop-single.js';
import './desktop/desktop-front.js';
import './desktop/desktop-category.js';

import './mobile/mobile-main.js';
import './mobile/mobile-single.js';
import './mobile/mobile-front.js';
import './mobile/mobile-category.js';