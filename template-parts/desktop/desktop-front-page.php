<div class="desktop-front-page">

  <div class="wrap mb-5">
    <?php np_template_desktop('front-section-1'); ?>
  </div>

  <div class="wrap mb-5">
    <div class="flex">
      <div class="w-[70%] pr-3">
        <?php np_template_desktop('front-section-2'); ?>
      </div>
      <div class="w-[30%] pl-3">
        <?php np_template_desktop('front-section-3'); ?>
      </div>
    </div>
  </div>

  <div class="wrap mb-5">
    <div class="ad-container"> <?php
      echo np_get_ad_1(); ?>
    </div>
  </div>

  <div class="wrap mb-5"> <?php
    np_template_desktop('front-section-4'); ?>
  </div>

  <div class="wrap mb-5">
    <div class="ad-container"> <?php
      echo np_get_ad_2(); ?>
    </div>
  </div>
</div>
