<?php
  $desktop_category_section_3 = np_get_popular_posts(5, 1);
  if (count($desktop_category_section_3) < 5) {
    $desktop_category_section_3 = np_get_latest_posts(5, 1);
  }
?>

<div class="desktop-category-section-3 border-black"><?php
  echo np_template_desktop('sidebar-head-1', [
    'html_title' => '<span class="text-red-500">인기 급상승</span> 뉴스',
  ]); ?>

  <div class="swiper">
    <div class="swiper-wrapper"> <?php
      for ($i = 0; $i < count($desktop_category_section_3); $i += 1) {
        $post = $desktop_category_section_3[$i]; ?>
        <div class="swiper-slide">
          <a href="<?php echo $post['post_link']; ?>" class="flex flex-col relative mb-2">
            <img src="<?php echo np_get_image_url($post['thumbnail_image_id']); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>" class="aspect-[8/5]">
            <h2 class="absolute bottom-3 left-0 right-0 z-20 px-2 font-normal text-base text-white line-clamp-2 leading-[24px] max-h-[52px]">
              <?php echo $post['title']; ?>
            </h3>
            <div class="title-cover h-[100px] z-10"></div>
          </a>
        </div>
      <?php } ?>
    </div>
    <div class="swiper-pagination relative"></div>
  </div>
</div>

