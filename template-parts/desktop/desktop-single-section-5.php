<?php
$post_id = $args['post_id'];
$email = '';
$author_id = get_the_author_meta('ID');
$author_email = get_the_author_meta('user_email', $author_id);

$category_ids = wp_get_post_categories($post_id);
if (is_array($category_ids) && count($category_ids) > 0) {
  $category_id = $category_ids[0];
  $category_link = get_category_link($category_id);
} ?>

<div class="desktop-single-section-5">
  <div class="flex justify-between items-center border-b border-b-black border-t py-5">
    <div class="flex items-center">
      <img class="h-10 w-10 rounded-full"
        src="https://cdn.salgoonews.com/news/photo/member/keemyesl_20220904041020.jpg" alt="">
      <strong class="mx-2"><?php the_author(); ?> 기자</strong>
      <a href="mailto:<?php echo $author_email; ?>"
        class="text-xs text-slate-500 hover:underline "><?php echo $author_email; ?></a>
    </div>
    <div>
      <?php
        if (isset($category_link)) { ?>
          <a href="<?php echo $category_link; ?>" class="text-xs text-slate-500">다른기사 보기</a> <?php
        } ?>
    </div>
  </div>
</div>
