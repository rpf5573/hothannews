<?php
  $desktop_front_section_3 = np_get_popular_posts(10, 1 * 24);
  if (count($desktop_front_section_3) < 10) {
    $desktop_front_section_3 = np_get_latest_posts(10);
  }
  if (count($desktop_front_section_3) < 10) {
    echo "글 개수가 부족합니다. 10개 이상 입력해 주세요";
    return;
  }
?>

<div class="desktop-front-section-3 border-black">
  <header class="border-b-2 border-b-black mb-2"> <?php
    np_template_desktop('sidebar-head-1', [
      'html_title' => '오늘의 <span class="text-red-500">베스트 10</span>',
      'h1_class' => '!pb-1 !mb-0',
    ]); ?>
  </header>
  <div class="swiper">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <ul> <?php
          for ($i = 0; $i < 5; $i += 1) {
            $post = $desktop_front_section_3[$i]; ?>
            <li class="mb-4">
              <a href="<?php echo $post['post_link']; ?>" class="flex hover:underline">
                <p class="font-bold align-top w-8 <?php if ($i < 3) { echo "text-red-500"; } ?>"><?php echo $i + 1; ?></p>
                <div class="align-top flex-1">
                  <h2 class="text-base pr-2 line-clamp-3"><?php echo $post['title']; ?></h2>
                </div>
                <p class="w-[20%]">
                  <img src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-150x150'); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>">
                </p>
              </a>
            </li> <?php
          } ?>
        </ul>
      </div>
      <div class="swiper-slide">
        <ul> <?php
          for ($i = 5; $i < 10; $i += 1) {
            $post = $desktop_front_section_3[$i]; ?>
            <li class="mb-4">
              <a href="<?php echo $post['post_link']; ?>" class="flex hover:underline">
                <p class="font-bold align-top w-8 <?php if ($i < 3) { echo "text-red-500"; } ?>"><?php echo $i + 1; ?></p>
                <div class="align-top flex-1">
                  <h2 class="text-base pr-2 line-clamp-3"><?php echo $post['title']; ?></h2>
                </div>
                <p class="w-[20%]">
                  <img src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-150x150'); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>">
                </p>
              </a>
            </li> <?php
          } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
