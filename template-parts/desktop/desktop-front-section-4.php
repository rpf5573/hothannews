<?php
$desktop_front_section_4 = np_get_latest_posts(16); ?>

<div class="desktop-front-section-4">
  <ul class="grid grid-cols-4 gap-2 mb-3"> <?php
    foreach($desktop_front_section_4 as $post) {
      np_template_desktop('front-section-4-list-item', ['post' => $post]);
    }
  ?>
  </ul>
  <div class="flex justify-center">
    <button class="load-more desktop" data-current-page="1">
      <span>더보기</span>
    </button>
    <div class="no-more-posts desktop hidden">
      마지막 페이지입니다
    </div>
  </div>
</div>
