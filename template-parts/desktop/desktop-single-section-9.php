<?php
$desktop_single_section_9 = np_get_latest_posts(5); ?>

<div class="desktop-single-section-9"> <?php
  echo np_template_desktop('sidebar-head-1', [
    'html_title' => '최신 뉴스',
    'h1_class' => '!pb-3'
  ]); ?>
  <ul class="list-disc pl-[20px]"> <?php
    for ($i = 0; $i < count($desktop_single_section_9); $i += 1) { ?>
      <li>
        <a href="<?php echo $desktop_single_section_9[$i]['post_link']; ?>" class="truncate block hover:underline text-sm my-2"> <?php
          echo $desktop_single_section_9[$i]['title']; ?>
        </a>
      </li><?php
    } ?>
  </ul>
</div>
