<header class="desktop-single-section-1 entry-header border-b mb-10">
  <nav>
    <?php echo np_category_breadcrumbs(get_the_ID()); ?>
  </nav>
  <h1 class="text-4xl font-bold my-5 leading-normal">
    <?php the_title(); ?>
  </h1>
  <div class="flex justify-start items-center mb-4">
    <article>
      <ul class="flex gap-x-3 text-xs text-slate-500">
        <li><i class="icon-user-o"></i> <span><?php echo get_the_author(); ?> 기자</span></li>
        <li>|</li>
        <li><i class="icon-clock-o"></i> <span><?php echo get_the_date( 'Y.m.d H:i' ); ?></span> </li>
      </ul>
    </article>
  </div>
</header>
