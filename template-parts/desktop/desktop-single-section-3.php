<?php
$post_id = get_queried_object_id();
?>

<div class="desktop-single-section-3 article-single-reaction"> <?php
  np_template_common('post-reaction', [
    'post_id' => $post_id
  ]); ?>
</div>

