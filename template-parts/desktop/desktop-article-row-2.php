<?php
/**
 * Template part for displaying article 2
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package newspaper
 */

?>
<div class="article-row-2 <?php echo $args['class']; ?>">
  <a class="flex flex-wrap" href="<?php echo $args['link_url']; ?>">
    <div class="grow-0 shrink-0 basis-auto w-4/12 img-hover-scale-1">
      <img src="<?php echo $args['img_url']; ?>" class="aspect-[4/3] <?php echo isset($args['aspect']) ? $args['aspect'] : ''; ?>" alt="<?php echo $args['img_alt']; ?>" />
    </div>
    <div class="flex flex-col justify-between grow-0 shrink-0 basis-auto w-8/12 pl-3">
      <h2 class="text-sm line-clamp-3 leading-[18px] max-h-[50px] lg:text-base lg:leading-[24px] lg:max-h-[72px]"><?php echo $args['title']; ?></h3>
      <span class="text-xs text-gray-300 pb-2"><?php echo $args['date']; ?></span>
    </div>
  </a>
</div>
