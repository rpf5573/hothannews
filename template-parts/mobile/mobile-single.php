<div class="mobile-single">

  <?php
  // The WordPress Loop: let's retrieve our post.
  while ( have_posts() ) : the_post();
    $post_id = get_the_ID(); ?>

    <article id="post-<?php echo $post_id; ?>" <?php post_class(); ?>>
      <!-- 헤더 -->
      <div class="wrap"> <?php
        np_template_mobile('single-section-1'); ?>
      </div>

      <!-- 본문 -->
      <div class="wrap">
        <section>
          <div class="mb-5"> <?php
            np_template_mobile('single-section-2'); ?>
          </div>
          <div class="mb-5 py-10 border-t border-b"> <?php
            np_template_mobile('single-section-3'); ?>
          </div>
          <div class="mb-5"> <?php
            np_template_mobile('single-section-4'); ?>
          </div>
          <div class="mb-10"> <?php
            np_template_mobile('single-section-5'); ?>
          </div>
          <div class="mb-10"> <?php
            np_template_mobile('single-section-6', [
              'post_id' => $post_id
            ]); ?>
          </div>
          <div class="mb-10"> <?php
            np_template_mobile('single-section-7'); ?>
          </div>
        </section>
      </div>
    </article> <?php
  endwhile;
  rewind_posts(); ?>
</div>
