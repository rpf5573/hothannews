<?php
$h1_class_list = isset($args['h1_class_list']) ? (is_array($args['h1_class_list']) ? $args['h1_class_list'] : [] ) : [];
$html_title = isset($args['html_title']) ? $args['html_title'] : ''; ?>

<header>
  <h1 class="text-xl font-bold py-3 flex items-center justify-between <?php echo implode(' ', $h1_class_list); ?>">
    <div class="left"> <?php
      echo $html_title; ?>
    </div>
    <div class="swiper-pagination grow-0 shrink basis-0 !static text-end pr-2 !justify-end"></div>
  </h1>
</header>