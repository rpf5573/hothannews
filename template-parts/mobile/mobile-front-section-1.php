<?php 
$mobile_front_section_1 = np_get_latest_posts(5); ?>

<div class="mobile-front-section-1">
  <div class="swiper"> 
    <div class="swiper-wrapper"> <?php
      for ($i = 0; $i < count($mobile_front_section_1); $i += 1) {
        $post = $mobile_front_section_1[$i]; ?>
        <div class="swiper-slide">
          <a href="<?php echo $post['post_link']; ?>" class="flex flex-col">
            <img src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-400x300'); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>" class="mb-2 aspect-[4/3]">
            <h2 class="font-normal text-lg line-clamp-2 leading-[28px] max-h-[90px]"><?php echo $post['title']; ?></h3>
          </a>
        </div>
      <?php } ?>
    </div>
    <div class="swiper-pagination !top-0 !left-0 !right-0 !bottom-[auto] text-end pr-2"></div>
  </div>
</div>
