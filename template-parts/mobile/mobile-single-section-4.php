<?php
$mobile_single_section_4 = np_get_latest_posts(10);
if (count($mobile_single_section_4) < 10) {
  echo "글을 더 입력해 주세요";
  return;
} ?>

<div class="mobile-single-section-4"> <?php
  np_template_mobile('head-1', [
    'html_title' => '<span class="text-red-500">당신이 좋아할 만한 뉴스</span>',
    'pagination_count' => 2
  ]); ?>
  <div class="swiper">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <div class="flex flex-col"> <?php
          for ($i = 0; $i < 5; $i += 1) {
            $post = $mobile_single_section_4[$i]; ?>
            <a href="<?php echo $post['post_link']; ?>" class="mb-2">
              <h2><?php echo $post['title']; ?></h3>
            </a> <?php
          } ?>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="flex flex-col"> <?php
          for ($i = 5; $i < 10; $i += 1) {
            $post = $mobile_single_section_4[$i]; ?>
            <a href="<?php echo $post['post_link']; ?>" class="mb-2">
              <h2><?php echo $post['title']; ?></h3>
            </a> <?php
          } ?>
        </div>
      </div>
    </div>
  </div>
</div>
