<?php
/**
 * Template part for displaying article 1
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package newspaper
 */

?>
<div class="row-article-1 <?php echo $args['class']; ?>">
  <a class="flex flex-wrap" href="<?php echo $args['link_url']; ?>">
    <div class="grow-0 shrink-0 basis-auto w-3/12 overflow-hidden">
      <img src="<?php echo $args['img_url']; ?>" class="img-hover-scale-1 <?php echo $args['aspect']; ?> aspect-[1/1]" alt="<?php echo $args['img_alt']; ?>" />
    </div>
    <div class="flex flex-col justify-between grow-0 shrink-0 basis-auto w-9/12 pl-2">
      <h2 class="line-clamp-2 text-sm leading-[20px] max-h-[50px]"><?php echo $args['title']; ?></h2>
      <span class="category text-xs text-red-600">이슈</span>
    </div>
  </a>
</div>
