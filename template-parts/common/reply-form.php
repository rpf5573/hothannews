<?php
  $post_id = $args['post_id'];
  $parent_comment_id = $args['parent_comment_id'];
?>

<form class="reply-form border bg-white rounded-lg">
  <div class="p-4">
    <div class="md:flex md:justify-between md:mb-4">
      <p class="mb-2 md:mb-0">댓글 작성하기</p>
      <div class="grid grid-cols-2 gap-1 mb-2 md:mb-0 md:flex">
        <input name="username" type="text" class="text-center border rounded-full !text-xs !p-2 !h-7 md:w-28" placeholder="작성자" required>
        <input name="password" type="text" class="text-center border rounded-full !text-xs !p-2 !h-7 md:w-28" placeholder="비밀번호" required>
        <input type="hidden" name="post_id" value="<?php echo $post_id; ?>" />
        <input type="hidden" name="parent_comment_id" value="<?php echo $parent_comment_id; ?>" />
        <input type="hidden" name="reply_nonce" value="<?php echo wp_create_nonce('add_reply_nonce'); ?>">
      </div>
    </div>
    <textarea name="content" class="w-full p-3 text-xs" cols="30" rows="3" required style="resize: none"
      placeholder="권리침해, 욕설 및 특정 대상을 비하하는 내용을 게시할 경우 이용약관 및 관련법률에 의해 제재될 수 있습니다."></textarea>
  </div>
  <div class="border-t flex justify-between items-center p-2 md:p-3">
    <p class="letter-counter text-sm"><span class="current-count">0</span>/<span class="text-slate-400">400</span></p>
    <button type="submit" class="bg-red-500 px-3 py-2 text-xs text-white rounded-md md:px-4">등록</button>
  </div>
</form>
