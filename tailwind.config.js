const theme = require('./theme.json');
const tailpress = require("@jeffreyvr/tailwindcss-tailpress");

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './*.php',
        './**/*.php',
        './assets/css/*.css',
        './assets/js/*.js',
        './safelist.txt'
    ],
    theme: {
    },
    plugins: [
        tailpress.tailwind
    ]
};
