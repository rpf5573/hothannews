<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package testtest
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <script src="https://unpkg.com/feather-icons"></script>
  
  <!-- 광고 코드 -->
  <!-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2280343098115601" crossorigin="anonymous"></script> -->
  <meta name="naver-site-verification" content="834be9b569c396a484a3a30c27e72d20674a5d87" />

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?php wp_body_open(); ?>
  <div id="page" class="site">
    <header id="header" class="site-header">
      <div class="wrap">
        <div
          class="flex justify-between h-[80px] relative items-center xl:flex xl:justify-between xl:h-[120px] xl:relative xl:items-center">
          <div id="offset-menu-trigger">
            <div class="hamburger type2">
              <div class="menu_icon">
                <span class="border border1"></span>
                <span class="border border2"></span>
                <span class="border border3"></span>
              </div>
            </div>
          </div>
          <div class="site-logo">
            <a href="<?php echo home_url(); ?>"><img src="<?php echo np_get_site_logo_url(); ?>" alt="site-logo" /></a>
          </div>
          <div class="social-icons hidden md:block"> <?php
            echo do_shortcode( '[korea_sns_pro_button]' ); ?>
          </div>
        </div>
      </div><!-- #header .wrap -->
    </header> <?php
      if (has_nav_menu('primary-menu')) {
        wp_nav_menu(
          array(
            'container' => 'nav',
            'container_class' => 'header-primary-menu',
            'menu_class' => 'menu wrap',
            'theme_location' => 'primary-menu',
            'fallback_cb' => false
          )
        );

        wp_nav_menu(
          array(
            'container' => 'nav',
            'container_class' => 'offset-menu',
            'menu_class' => 'menu hidden', // 처음에 hidden을 붙였다가 나중에 땐다..가 아니라 안때도 되네!
            'theme_location' => 'primary-menu',
            'fallback_cb' => false
          )
        );
      } ?>

